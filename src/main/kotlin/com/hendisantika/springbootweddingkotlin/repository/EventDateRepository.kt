package com.hendisantika.springbootweddingkotlin.repository

import com.hendisantika.springbootweddingkotlin.entity.DateType
import com.hendisantika.springbootweddingkotlin.entity.EventDate
import org.springframework.data.domain.Example
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.data.domain.Sort
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Modifying
import org.springframework.data.jpa.repository.Query
import org.springframework.security.access.annotation.Secured

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-wedding-kotlin
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-03-06
 * Time: 19:11
 */
interface EventDateRepository : JpaRepository<EventDate, Long> {
    fun getByDateType(dateType: DateType): EventDate?

    @Secured("ROLE_ADMIN", "ROLE_USER")
    @Modifying
    @Query("DELETE FROM EventDate ede where ede.id in (?1)")
    fun deleteAll(ids: LongArray): Int

    @Secured("ROLE_ADMIN", "ROLE_USER")
    override fun deleteAll()

    @Secured("ROLE_ADMIN", "ROLE_USER")
    override fun getOne(p0: Long?): EventDate

    override fun <S : EventDate?> findAll(p0: Example<S>?, p1: Sort?): MutableList<S>

    override fun findAll(): MutableList<EventDate>

    override fun findAll(p0: MutableIterable<Long>?): MutableList<EventDate>

    override fun findAll(p0: Sort?): MutableList<EventDate>

    override fun findAll(p0: Pageable?): Page<EventDate>

    override fun <S : EventDate?> findAll(p0: Example<S>?): MutableList<S>

    override fun <S : EventDate?> findAll(p0: Example<S>?, p1: Pageable?): Page<S>

    @Secured("ROLE_ADMIN", "ROLE_USER")
    override fun <S : EventDate?> exists(p0: Example<S>?): Boolean

    @Secured("ROLE_ADMIN", "ROLE_USER")
    override fun exists(p0: Long?): Boolean

    @Secured("ROLE_ADMIN", "ROLE_USER")
    override fun <S : EventDate?> count(p0: Example<S>?): Long

    @Secured("ROLE_ADMIN", "ROLE_USER")
    override fun count(): Long

    @Secured("ROLE_ADMIN", "ROLE_USER")
    override fun delete(p0: Long?)

    @Secured("ROLE_ADMIN", "ROLE_USER")
    override fun delete(p0: EventDate?)

    @Secured("ROLE_ADMIN", "ROLE_USER")
    override fun delete(p0: MutableIterable<EventDate>?)

    @Secured("ROLE_ADMIN", "ROLE_USER")
    override fun deleteInBatch(p0: MutableIterable<EventDate>?)

    @Secured("ROLE_ADMIN", "ROLE_USER")
    override fun <S : EventDate?> save(p0: MutableIterable<S>?): MutableList<S>

    @Secured("ROLE_ADMIN", "ROLE_USER")
    override fun <S : EventDate?> save(p0: S): S

    @Secured("ROLE_ADMIN", "ROLE_USER")
    override fun flush()

    @Secured("ROLE_ADMIN", "ROLE_USER")
    override fun <S : EventDate?> saveAndFlush(p0: S): S

    @Secured("ROLE_ADMIN", "ROLE_USER")
    override fun deleteAllInBatch()

    @Secured("ROLE_ADMIN", "ROLE_USER")
    override fun findOne(p0: Long?): EventDate

    @Secured("ROLE_ADMIN", "ROLE_USER")
    override fun <S : EventDate?> findOne(p0: Example<S>?): S
}