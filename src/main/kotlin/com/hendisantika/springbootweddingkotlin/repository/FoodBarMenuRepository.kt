package com.hendisantika.springbootweddingkotlin.repository

import com.hendisantika.springbootweddingkotlin.entity.FoodBarMenu
import org.springframework.data.domain.Example
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.data.domain.Sort
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.security.access.annotation.Secured

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-wedding-kotlin
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-03-06
 * Time: 19:15
 */
interface FoodBarMenuRepository : JpaRepository<FoodBarMenu, Long> {
    @Secured("ROLE_ADMIN", "ROLE_USER")
    override fun <S : FoodBarMenu?> findOne(p0: Example<S>?): S

    @Secured("ROLE_ADMIN", "ROLE_USER")
    override fun findOne(p0: Long?): FoodBarMenu

    @Secured("ROLE_ADMIN", "ROLE_USER")
    override fun <S : FoodBarMenu?> save(p0: MutableIterable<S>?): MutableList<S>

    @Secured("ROLE_ADMIN", "ROLE_USER")
    override fun <S : FoodBarMenu?> save(p0: S): S

    @Secured("ROLE_ADMIN", "ROLE_USER")
    override fun delete(p0: MutableIterable<FoodBarMenu>?)

    @Secured("ROLE_ADMIN", "ROLE_USER")
    override fun delete(p0: Long?)

    @Secured("ROLE_ADMIN", "ROLE_USER")
    override fun delete(p0: FoodBarMenu?)

    @Secured("ROLE_ADMIN", "ROLE_USER")
    override fun getOne(p0: Long?): FoodBarMenu

    @Secured("ROLE_ADMIN", "ROLE_USER")
    override fun deleteInBatch(p0: MutableIterable<FoodBarMenu>?)

    override fun findAll(p0: Pageable?): Page<FoodBarMenu>

    override fun <S : FoodBarMenu?> findAll(p0: Example<S>?, p1: Sort?): MutableList<S>

    override fun findAll(p0: MutableIterable<Long>?): MutableList<FoodBarMenu>

    override fun <S : FoodBarMenu?> findAll(p0: Example<S>?, p1: Pageable?): Page<S>

    override fun <S : FoodBarMenu?> findAll(p0: Example<S>?): MutableList<S>

    override fun findAll(): MutableList<FoodBarMenu>

    override fun findAll(p0: Sort?): MutableList<FoodBarMenu>

    @Secured("ROLE_ADMIN", "ROLE_USER")
    override fun <S : FoodBarMenu?> saveAndFlush(p0: S): S

    @Secured("ROLE_ADMIN", "ROLE_USER")
    override fun <S : FoodBarMenu?> count(p0: Example<S>?): Long

    @Secured("ROLE_ADMIN", "ROLE_USER")
    override fun count(): Long

    @Secured("ROLE_ADMIN", "ROLE_USER")
    override fun deleteAll()

    @Secured("ROLE_ADMIN", "ROLE_USER")
    override fun exists(p0: Long?): Boolean

    @Secured("ROLE_ADMIN", "ROLE_USER")
    override fun <S : FoodBarMenu?> exists(p0: Example<S>?): Boolean

    @Secured("ROLE_ADMIN", "ROLE_USER")
    override fun flush()

    @Secured("ROLE_ADMIN", "ROLE_USER")
    override fun deleteAllInBatch()
}