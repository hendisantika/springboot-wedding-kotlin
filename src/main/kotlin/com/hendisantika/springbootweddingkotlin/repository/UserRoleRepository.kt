package com.hendisantika.springbootweddingkotlin.repository

import com.hendisantika.springbootweddingkotlin.entity.UserRole
import org.springframework.data.domain.Example
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.data.domain.Sort
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Modifying
import org.springframework.data.jpa.repository.Query
import org.springframework.security.access.annotation.Secured

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-wedding-kotlin
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-03-06
 * Time: 19:05
 */
interface UserRoleRepository : JpaRepository<UserRole, Long> {
    @Secured("ROLE_ADMIN")
    @Modifying
    @Query("DELETE FROM UserRole re where re.id in (?1)")
    fun deleteAll(ids: LongArray): Int

    @Secured("ROLE_ADMIN")
    override fun deleteAll()

    @Secured("ROLE_ADMIN")
    override fun delete(p0: MutableIterable<UserRole>?)

    override fun findAll(p0: Pageable?): Page<UserRole>

    override fun findAll(p0: MutableIterable<Long>?): MutableList<UserRole>

    override fun findAll(p0: Sort?): MutableList<UserRole>

    override fun findAll(): MutableList<UserRole>

    @Secured("ROLE_ADMIN")
    override fun deleteAllInBatch()

    @Secured("ROLE_ADMIN")
    override fun flush()

    @Secured("ROLE_ADMIN")
    override fun deleteInBatch(p0: MutableIterable<UserRole>?)

    @Secured("ROLE_ADMIN")
    override fun count(): Long

    @Secured("ROLE_ADMIN")
    override fun delete(p0: Long?)

    @Secured("ROLE_ADMIN")
    override fun delete(p0: UserRole?)

    override fun <S : UserRole?> findAll(p0: Example<S>?): MutableList<S>

    override fun <S : UserRole?> findAll(p0: Example<S>?, p1: Pageable?): Page<S>

    override fun <S : UserRole?> findAll(p0: Example<S>?, p1: Sort?): MutableList<S>

    @Secured("ROLE_ADMIN")
    override fun <S : UserRole?> count(p0: Example<S>?): Long

    @Secured("ROLE_ADMIN")
    override fun <S : UserRole?> save(p0: MutableIterable<S>?): MutableList<S>

    @Secured("ROLE_ADMIN")
    override fun <S : UserRole?> save(p0: S): S

    @Secured("ROLE_ADMIN")
    override fun getOne(p0: Long?): UserRole

    @Secured("ROLE_ADMIN")
    override fun <S : UserRole?> findOne(p0: Example<S>?): S

    @Secured("ROLE_ADMIN")
    override fun findOne(p0: Long?): UserRole

    @Secured("ROLE_ADMIN")
    override fun exists(p0: Long?): Boolean

    @Secured("ROLE_ADMIN")
    override fun <S : UserRole?> exists(p0: Example<S>?): Boolean

    @Secured("ROLE_ADMIN")
    override fun <S : UserRole?> saveAndFlush(p0: S): S
}