package com.hendisantika.springbootweddingkotlin.repository

import com.hendisantika.springbootweddingkotlin.entity.Registry
import org.springframework.data.domain.Example
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.data.domain.Sort
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.security.access.annotation.Secured

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-wedding-kotlin
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-03-06
 * Time: 19:17
 */
interface RegistryRepository : JpaRepository<Registry, Long> {
    @Secured("ROLE_ADMIN", "ROLE_USER")
    override fun exists(p0: Long?): Boolean

    @Secured("ROLE_ADMIN", "ROLE_USER")
    override fun <S : Registry?> exists(p0: Example<S>?): Boolean

    @Secured("ROLE_ADMIN", "ROLE_USER")
    override fun <S : Registry?> saveAndFlush(p0: S): S

    override fun <S : Registry?> findAll(p0: Example<S>?): MutableList<S>

    override fun findAll(p0: Pageable?): Page<Registry>

    override fun <S : Registry?> findAll(p0: Example<S>?, p1: Sort?): MutableList<S>

    override fun <S : Registry?> findAll(p0: Example<S>?, p1: Pageable?): Page<S>

    override fun findAll(): MutableList<Registry>

    override fun findAll(p0: MutableIterable<Long>?): MutableList<Registry>

    override fun findAll(p0: Sort?): MutableList<Registry>

    @Secured("ROLE_ADMIN", "ROLE_USER")
    override fun findOne(p0: Long?): Registry

    @Secured("ROLE_ADMIN", "ROLE_USER")
    override fun <S : Registry?> findOne(p0: Example<S>?): S

    @Secured("ROLE_ADMIN", "ROLE_USER")
    override fun deleteInBatch(p0: MutableIterable<Registry>?)

    @Secured("ROLE_ADMIN", "ROLE_USER")
    override fun deleteAll()

    @Secured("ROLE_ADMIN", "ROLE_USER")
    override fun delete(p0: Long?)

    @Secured("ROLE_ADMIN", "ROLE_USER")
    override fun delete(p0: MutableIterable<Registry>?)

    @Secured("ROLE_ADMIN", "ROLE_USER")
    override fun delete(p0: Registry?)

    @Secured("ROLE_ADMIN", "ROLE_USER")
    override fun <S : Registry?> save(p0: MutableIterable<S>?): MutableList<S>

    @Secured("ROLE_ADMIN", "ROLE_USER")
    override fun <S : Registry?> save(p0: S): S

    @Secured("ROLE_ADMIN", "ROLE_USER")
    override fun <S : Registry?> count(p0: Example<S>?): Long

    @Secured("ROLE_ADMIN", "ROLE_USER")
    override fun count(): Long

    @Secured("ROLE_ADMIN", "ROLE_USER")
    override fun getOne(p0: Long?): Registry

    @Secured("ROLE_ADMIN", "ROLE_USER")
    override fun deleteAllInBatch()

    @Secured("ROLE_ADMIN", "ROLE_USER")
    override fun flush()
}