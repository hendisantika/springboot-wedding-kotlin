package com.hendisantika.springbootweddingkotlin.repository

import com.hendisantika.springbootweddingkotlin.entity.WeddingThemeContent
import org.springframework.data.domain.Example
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.data.domain.Sort
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.security.access.annotation.Secured
import javax.transaction.Transactional

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-wedding-kotlin
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-03-06
 * Time: 19:14
 */
interface WeddingThemeContentRepository : JpaRepository<WeddingThemeContent, Long> {
    @Transactional(Transactional.TxType.REQUIRES_NEW)
    override fun findAll(p0: Pageable?): Page<WeddingThemeContent>

    @Transactional(Transactional.TxType.REQUIRES_NEW)
    override fun findAll(p0: Sort?): MutableList<WeddingThemeContent>

    @Transactional(Transactional.TxType.REQUIRES_NEW)
    override fun <S : WeddingThemeContent?> findAll(p0: Example<S>?): MutableList<S>

    @Transactional(Transactional.TxType.REQUIRES_NEW)
    override fun findAll(p0: MutableIterable<Long>?): MutableList<WeddingThemeContent>

    @Transactional(Transactional.TxType.REQUIRES_NEW)
    override fun <S : WeddingThemeContent?> findAll(p0: Example<S>?, p1: Pageable?): Page<S>

    @Transactional(Transactional.TxType.REQUIRES_NEW)
    override fun <S : WeddingThemeContent?> findAll(p0: Example<S>?, p1: Sort?): MutableList<S>

    @Transactional(Transactional.TxType.REQUIRES_NEW)
    override fun findAll(): MutableList<WeddingThemeContent>

    @Secured("ROLE_ADMIN", "ROLE_USER")
    override fun delete(p0: MutableIterable<WeddingThemeContent>?)

    @Secured("ROLE_ADMIN", "ROLE_USER")
    override fun delete(p0: WeddingThemeContent?)

    @Secured("ROLE_ADMIN", "ROLE_USER")
    override fun delete(p0: Long?)

    @Secured("ROLE_ADMIN", "ROLE_USER")
    override fun deleteAll()

    @Secured("ROLE_ADMIN", "ROLE_USER")
    override fun count(): Long

    @Secured("ROLE_ADMIN", "ROLE_USER")
    override fun <S : WeddingThemeContent?> count(p0: Example<S>?): Long

    @Secured("ROLE_ADMIN", "ROLE_USER")
    @Transactional(Transactional.TxType.REQUIRES_NEW)
    override fun <S : WeddingThemeContent?> save(p0: S): S

    @Secured("ROLE_ADMIN", "ROLE_USER")
    @Transactional(Transactional.TxType.REQUIRES_NEW)
    override fun <S : WeddingThemeContent?> save(p0: MutableIterable<S>?): MutableList<S>

    @Secured("ROLE_ADMIN", "ROLE_USER")
    @Transactional(Transactional.TxType.REQUIRES_NEW)
    override fun <S : WeddingThemeContent?> saveAndFlush(p0: S): S

    @Secured("ROLE_ADMIN", "ROLE_USER")
    override fun exists(p0: Long?): Boolean

    @Secured("ROLE_ADMIN", "ROLE_USER")
    override fun <S : WeddingThemeContent?> exists(p0: Example<S>?): Boolean

    @Secured("ROLE_ADMIN", "ROLE_USER")
    override fun findOne(p0: Long?): WeddingThemeContent

    @Secured("ROLE_ADMIN", "ROLE_USER")
    override fun <S : WeddingThemeContent?> findOne(p0: Example<S>?): S

    @Secured("ROLE_ADMIN", "ROLE_USER")
    override fun getOne(p0: Long?): WeddingThemeContent

    @Secured("ROLE_ADMIN", "ROLE_USER")
    override fun flush()

    @Secured("ROLE_ADMIN", "ROLE_USER")
    override fun deleteAllInBatch()

    @Secured("ROLE_ADMIN", "ROLE_USER")
    override fun deleteInBatch(p0: MutableIterable<WeddingThemeContent>?)
}