package com.hendisantika.springbootweddingkotlin.repository

import com.hendisantika.springbootweddingkotlin.entity.SiteUser
import org.springframework.data.domain.Example
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.data.domain.Sort
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Modifying
import org.springframework.data.jpa.repository.Query
import org.springframework.security.access.annotation.Secured

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-wedding-kotlin
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-03-06
 * Time: 19:08
 */
interface SiteUserRepository : JpaRepository<SiteUser, Long> {
    //Needs to be publicly accessible logging into the website
    fun getByUserName(userName: String): SiteUser?

    @Secured("ROLE_ADMIN")
    @Modifying
    @Query("DELETE FROM SiteUser se where se.id in (?1)")
    fun deleteAll(ids: LongArray): Int

    @Secured("ROLE_ADMIN")
    fun countByUserName(userName: String): Long

    @Secured("ROLE_ADMIN")
    fun countByEmail(email: String): Long

    @Secured("ROLE_ADMIN")
    override fun deleteAll()

    @Secured("ROLE_ADMIN")
    override fun flush()

    @Secured("ROLE_ADMIN")
    override fun deleteInBatch(p0: MutableIterable<SiteUser>?)

    override fun findAll(p0: Sort?): MutableList<SiteUser>

    override fun <S : SiteUser?> findAll(p0: Example<S>?, p1: Pageable?): Page<S>

    override fun findAll(p0: Pageable?): Page<SiteUser>

    override fun findAll(): MutableList<SiteUser>

    override fun <S : SiteUser?> findAll(p0: Example<S>?, p1: Sort?): MutableList<S>

    override fun findAll(p0: MutableIterable<Long>?): MutableList<SiteUser>

    override fun <S : SiteUser?> findAll(p0: Example<S>?): MutableList<S>

    @Secured("ROLE_ADMIN")
    override fun <S : SiteUser?> save(p0: MutableIterable<S>?): MutableList<S>

    @Secured("ROLE_ADMIN")
    override fun <S : SiteUser?> save(p0: S): S

    @Secured("ROLE_ADMIN")
    override fun count(): Long

    @Secured("ROLE_ADMIN")
    override fun <S : SiteUser?> count(p0: Example<S>?): Long

    @Secured("ROLE_ADMIN")
    override fun delete(p0: Long?)

    @Secured("ROLE_ADMIN")
    override fun delete(p0: SiteUser?)

    @Secured("ROLE_ADMIN")
    override fun delete(p0: MutableIterable<SiteUser>?)

    @Secured("ROLE_ADMIN")
    override fun <S : SiteUser?> saveAndFlush(p0: S): S

    @Secured("ROLE_ADMIN")
    override fun deleteAllInBatch()

    @Secured("ROLE_ADMIN")
    override fun exists(p0: Long?): Boolean

    @Secured("ROLE_ADMIN")
    override fun <S : SiteUser?> exists(p0: Example<S>?): Boolean

    @Secured("ROLE_ADMIN")
    override fun <S : SiteUser?> findOne(p0: Example<S>?): S

    @Secured("ROLE_ADMIN")
    override fun findOne(p0: Long?): SiteUser

    @Secured("ROLE_ADMIN")
    override fun getOne(p0: Long?): SiteUser
}