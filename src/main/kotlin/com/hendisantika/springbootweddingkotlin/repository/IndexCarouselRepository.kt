package com.hendisantika.springbootweddingkotlin.repository

import com.hendisantika.springbootweddingkotlin.entity.IndexCarousel
import org.springframework.data.domain.Example
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.data.domain.Sort
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.security.access.annotation.Secured

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-wedding-kotlin
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-03-06
 * Time: 19:10
 */
interface IndexCarouselRepository : JpaRepository<IndexCarousel, Long> {
    @Secured("ROLE_ADMIN")
    fun countByDisplayOrder(value: Int): Long

    @Secured("ROLE_ADMIN")
    fun countByTitle(value: String): Long

    @Secured("ROLE_ADMIN")
    fun countByDestinationLink(value: String): Long

    @Secured("ROLE_ADMIN")
    override fun <S : IndexCarousel?> save(p0: MutableIterable<S>?): MutableList<S>

    @Secured("ROLE_ADMIN")
    override fun <S : IndexCarousel?> save(p0: S): S

    override fun findAll(): MutableList<IndexCarousel>

    @Secured("ROLE_ADMIN")
    override fun <S : IndexCarousel?> findAll(p0: Example<S>?, p1: Pageable?): Page<S>

    override fun findAll(p0: Sort?): MutableList<IndexCarousel>

    override fun findAll(p0: Pageable?): Page<IndexCarousel>

    override fun findAll(p0: MutableIterable<Long>?): MutableList<IndexCarousel>

    override fun <S : IndexCarousel?> findAll(p0: Example<S>?, p1: Sort?): MutableList<S>

    override fun <S : IndexCarousel?> findAll(p0: Example<S>?): MutableList<S>

    @Secured("ROLE_ADMIN")
    override fun <S : IndexCarousel?> findOne(p0: Example<S>?): S

    @Secured("ROLE_ADMIN")
    override fun findOne(p0: Long?): IndexCarousel

    @Secured("ROLE_ADMIN")
    override fun flush()

    @Secured("ROLE_ADMIN")
    override fun <S : IndexCarousel?> count(p0: Example<S>?): Long

    @Secured("ROLE_ADMIN")
    override fun count(): Long

    @Secured("ROLE_ADMIN")
    override fun <S : IndexCarousel?> saveAndFlush(p0: S): S

    @Secured("ROLE_ADMIN")
    override fun deleteAll()

    @Secured("ROLE_ADMIN")
    override fun exists(p0: Long?): Boolean

    @Secured("ROLE_ADMIN")
    override fun <S : IndexCarousel?> exists(p0: Example<S>?): Boolean

    @Secured("ROLE_ADMIN")
    override fun delete(p0: MutableIterable<IndexCarousel>?)

    @Secured("ROLE_ADMIN")
    override fun delete(p0: Long?)

    @Secured("ROLE_ADMIN")
    override fun delete(p0: IndexCarousel?)

    @Secured("ROLE_ADMIN")
    override fun deleteAllInBatch()

    @Secured("ROLE_ADMIN")
    override fun getOne(p0: Long?): IndexCarousel

    @Secured("ROLE_ADMIN")
    override fun deleteInBatch(p0: MutableIterable<IndexCarousel>?)
}