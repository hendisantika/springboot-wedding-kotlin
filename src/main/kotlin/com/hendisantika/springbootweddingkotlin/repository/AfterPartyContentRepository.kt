package com.hendisantika.springbootweddingkotlin.repository

import com.hendisantika.springbootweddingkotlin.entity.AfterPartyInfo
import org.springframework.data.domain.Example
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.data.domain.Sort
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.security.access.annotation.Secured

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-wedding-kotlin
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-03-06
 * Time: 19:16
 */
interface AfterPartyContentRepository : JpaRepository<AfterPartyInfo, Long> {
    @Secured("ROLE_ADMIN", "ROLE_USER")
    override fun deleteAll()

    @Secured("ROLE_ADMIN", "ROLE_USER")
    override fun <S : AfterPartyInfo?> saveAndFlush(p0: S): S

    @Secured("ROLE_ADMIN", "ROLE_USER")
    override fun <S : AfterPartyInfo?> exists(p0: Example<S>?): Boolean

    @Secured("ROLE_ADMIN", "ROLE_USER")
    override fun exists(p0: Long?): Boolean

    override fun findAll(p0: MutableIterable<Long>?): MutableList<AfterPartyInfo>

    override fun <S : AfterPartyInfo?> findAll(p0: Example<S>?, p1: Pageable?): Page<S>

    override fun findAll(p0: Sort?): MutableList<AfterPartyInfo>

    override fun findAll(): MutableList<AfterPartyInfo>

    override fun <S : AfterPartyInfo?> findAll(p0: Example<S>?): MutableList<S>

    override fun <S : AfterPartyInfo?> findAll(p0: Example<S>?, p1: Sort?): MutableList<S>

    override fun findAll(p0: Pageable?): Page<AfterPartyInfo>

    @Secured("ROLE_ADMIN", "ROLE_USER")
    override fun deleteAllInBatch()

    @Secured("ROLE_ADMIN", "ROLE_USER")
    override fun <S : AfterPartyInfo?> save(p0: S): S

    @Secured("ROLE_ADMIN", "ROLE_USER")
    override fun <S : AfterPartyInfo?> save(p0: MutableIterable<S>?): MutableList<S>

    @Secured("ROLE_ADMIN", "ROLE_USER")
    override fun delete(p0: MutableIterable<AfterPartyInfo>?)

    @Secured("ROLE_ADMIN", "ROLE_USER")
    override fun delete(p0: Long?)

    @Secured("ROLE_ADMIN", "ROLE_USER")
    override fun delete(p0: AfterPartyInfo?)

    @Secured("ROLE_ADMIN", "ROLE_USER")
    override fun count(): Long

    @Secured("ROLE_ADMIN", "ROLE_USER")
    override fun <S : AfterPartyInfo?> count(p0: Example<S>?): Long

    @Secured("ROLE_ADMIN", "ROLE_USER")
    override fun deleteInBatch(p0: MutableIterable<AfterPartyInfo>?)

    @Secured("ROLE_ADMIN", "ROLE_USER")
    override fun findOne(p0: Long?): AfterPartyInfo

    @Secured("ROLE_ADMIN", "ROLE_USER")
    override fun <S : AfterPartyInfo?> findOne(p0: Example<S>?): S

    @Secured("ROLE_ADMIN", "ROLE_USER")
    override fun flush()

    @Secured("ROLE_ADMIN", "ROLE_USER")
    override fun getOne(p0: Long?): AfterPartyInfo

}