package com.hendisantika.springbootweddingkotlin.repository

import com.hendisantika.springbootweddingkotlin.entity.WeddingVenueContent
import org.springframework.data.domain.Example
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.data.domain.Sort
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.security.access.annotation.Secured
import javax.transaction.Transactional

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-wedding-kotlin
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-03-06
 * Time: 19:12
 */
interface WeddingVenueContentRepository : JpaRepository<WeddingVenueContent, Long> {
    @Secured("ROLE_ADMIN", "ROLE_USER")
    override fun deleteInBatch(p0: MutableIterable<WeddingVenueContent>?)

    @Secured("ROLE_ADMIN", "ROLE_USER")
    @Transactional(Transactional.TxType.REQUIRES_NEW)
    override fun <S : WeddingVenueContent?> saveAndFlush(p0: S): S

    @Secured("ROLE_ADMIN", "ROLE_USER")
    override fun delete(p0: Long?)

    @Secured("ROLE_ADMIN", "ROLE_USER")
    override fun delete(p0: WeddingVenueContent?)

    @Secured("ROLE_ADMIN", "ROLE_USER")
    override fun delete(p0: MutableIterable<WeddingVenueContent>?)

    @Secured("ROLE_ADMIN", "ROLE_USER")
    override fun flush()

    @Transactional(Transactional.TxType.REQUIRES_NEW)
    override fun findAll(p0: Sort?): MutableList<WeddingVenueContent>

    @Transactional(Transactional.TxType.REQUIRES_NEW)
    override fun <S : WeddingVenueContent?> findAll(p0: Example<S>?): MutableList<S>

    @Transactional(Transactional.TxType.REQUIRES_NEW)
    override fun findAll(p0: MutableIterable<Long>?): MutableList<WeddingVenueContent>

    @Transactional(Transactional.TxType.REQUIRES_NEW)
    override fun findAll(): MutableList<WeddingVenueContent>

    @Transactional(Transactional.TxType.REQUIRES_NEW)
    override fun <S : WeddingVenueContent?> findAll(p0: Example<S>?, p1: Sort?): MutableList<S>

    @Transactional(Transactional.TxType.REQUIRES_NEW)
    override fun findAll(p0: Pageable?): Page<WeddingVenueContent>

    @Transactional(Transactional.TxType.REQUIRES_NEW)
    override fun <S : WeddingVenueContent?> findAll(p0: Example<S>?, p1: Pageable?): Page<S>

    @Secured("ROLE_ADMIN", "ROLE_USER")
    override fun findOne(p0: Long?): WeddingVenueContent

    @Secured("ROLE_ADMIN", "ROLE_USER")
    override fun <S : WeddingVenueContent?> findOne(p0: Example<S>?): S

    @Secured("ROLE_ADMIN", "ROLE_USER")
    override fun exists(p0: Long?): Boolean

    @Secured("ROLE_ADMIN", "ROLE_USER")
    override fun <S : WeddingVenueContent?> exists(p0: Example<S>?): Boolean

    @Secured("ROLE_ADMIN", "ROLE_USER")
    override fun getOne(p0: Long?): WeddingVenueContent

    @Secured("ROLE_ADMIN", "ROLE_USER")
    @Transactional(Transactional.TxType.REQUIRES_NEW)
    override fun <S : WeddingVenueContent?> save(p0: MutableIterable<S>?): MutableList<S>

    @Secured("ROLE_ADMIN", "ROLE_USER")
    @Transactional(Transactional.TxType.REQUIRES_NEW)
    override fun <S : WeddingVenueContent?> save(p0: S): S

    @Secured("ROLE_ADMIN", "ROLE_USER")
    override fun count(): Long

    @Secured("ROLE_ADMIN", "ROLE_USER")
    override fun <S : WeddingVenueContent?> count(p0: Example<S>?): Long

    @Secured("ROLE_ADMIN", "ROLE_USER")
    override fun deleteAll()

    @Secured("ROLE_ADMIN", "ROLE_USER")
    override fun deleteAllInBatch()
}