package com.hendisantika.springbootweddingkotlin.service

import com.hendisantika.springbootweddingkotlin.entity.SiteUser
import com.hendisantika.springbootweddingkotlin.repository.SiteUserRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.security.core.GrantedAuthority
import org.springframework.security.core.authority.SimpleGrantedAuthority
import org.springframework.security.core.userdetails.User
import org.springframework.security.core.userdetails.UserDetails
import org.springframework.security.core.userdetails.UserDetailsService
import org.springframework.security.core.userdetails.UsernameNotFoundException
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-wedding-kotlin
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-03-07
 * Time: 07:28
 */
@Service
@Transactional
class SiteUserService(@Autowired val siteUserRepository: SiteUserRepository, @Autowired private val userRoleService: UserRoleService) :
        UserDetailsService, SiteUserRepository by siteUserRepository, BulkDeleteService {

    private fun SiteUser.toUser(): User {
        val grantedAuthorities = mutableSetOf<GrantedAuthority>()
        roles.forEach { grantedAuthorities.add(SimpleGrantedAuthority("ROLE_${it.role}")) }
        return User(userName, password, enabled, accountNonExpired, credentialsNonExpired, accountNonLocked, grantedAuthorities)
    }

    private fun SiteUser.preparePersist() {
        password = BCryptPasswordEncoder().encode(password)
        roles = userRoleService.findAll(roleIds.toMutableList()).toMutableSet()
    }

    override fun loadUserByUsername(user: String): UserDetails {
        var userEntity = siteUserRepository.getByUserName(user)
        if (userEntity == null) {
            throw UsernameNotFoundException("No user found for $user")
        } else {
            return userEntity.toUser()
        }
    }


    override fun <S : SiteUser?> save(userEntity: S): S {
        userEntity?.preparePersist()
        return siteUserRepository.save(userEntity)
    }

    override fun <S : SiteUser?> save(entities: MutableIterable<S>?): MutableList<S> {
        entities?.forEach { it?.preparePersist() }
        return siteUserRepository.save(entities)
    }

    override fun <S : SiteUser?> saveAndFlush(entity: S): S {
        entity?.preparePersist()
        return siteUserRepository.saveAndFlush(entity)
    }
}
