package com.hendisantika.springbootweddingkotlin.service

import com.hendisantika.springbootweddingkotlin.repository.RegistryRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-wedding-kotlin
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-03-07
 * Time: 07:34
 */
@Service
@Transactional
class RegistryService(
        @Autowired
        private val registryRepository: RegistryRepository) :
        RegistryRepository by registryRepository, BulkDeleteService {

    override fun deleteAll(ids: LongArray): Int {
        val entities = findAll(ids.toMutableList())
        delete(entities)
        return ids.size
    }
}