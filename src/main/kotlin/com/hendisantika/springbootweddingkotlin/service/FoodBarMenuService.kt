package com.hendisantika.springbootweddingkotlin.service

import com.hendisantika.springbootweddingkotlin.entity.FoodBarMenu
import com.hendisantika.springbootweddingkotlin.entity.FoodBarMenuItem
import com.hendisantika.springbootweddingkotlin.repository.FoodBarMenuRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-wedding-kotlin
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-03-07
 * Time: 07:31
 */
@Service
@Transactional
class FoodBarMenuService(
        @Autowired
        private val foodBarMenuRepository: FoodBarMenuRepository) :
        FoodBarMenuRepository by foodBarMenuRepository, BulkDeleteService {

    private fun FoodBarMenu.parse() {
        menuItems = menuItemsStr.split('\n').map { FoodBarMenuItem(name = it.removeSurrounding("\n", "\n")) }.toMutableList()
    }

    override fun deleteAll(ids: LongArray): Int {
        val entities = findAll(ids.toMutableList())
        delete(entities)
        return ids.size
    }

    override fun <S : FoodBarMenu?> save(entity: S): S {
        entity?.parse()
        return foodBarMenuRepository.save(entity)
    }

    override fun <S : FoodBarMenu?> save(entities: MutableIterable<S>?): MutableList<S> {
        entities?.forEach { it?.parse() }
        return foodBarMenuRepository.save(entities)
    }

    override fun <S : FoodBarMenu?> saveAndFlush(entity: S): S {
        entity?.parse()
        return foodBarMenuRepository.saveAndFlush(entity)
    }
}
