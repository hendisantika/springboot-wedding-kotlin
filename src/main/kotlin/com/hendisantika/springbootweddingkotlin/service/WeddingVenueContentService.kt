package com.hendisantika.springbootweddingkotlin.service

import com.hendisantika.springbootweddingkotlin.entity.WeddingVenueContent
import com.hendisantika.springbootweddingkotlin.repository.WeddingVenueContentRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.PageRequest
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-wedding-kotlin
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-03-07
 * Time: 07:30
 */
@Service
@Transactional
class WeddingVenueContentService(
        @Autowired
        private val weddingVenueContentRepository: WeddingVenueContentRepository) :
        WeddingVenueContentRepository by weddingVenueContentRepository, BulkDeleteService {

    override fun deleteAll(ids: LongArray): Int {
        val weddingVenueContent = findOrCreate()
        weddingVenueContent.images.removeIf { it.id ?: -1 in ids }
        save(weddingVenueContent)
        return ids.size
    }

    fun findOrCreate(): WeddingVenueContent =
            findAll(PageRequest(0, 1)).elementAtOrElse(0, { WeddingVenueContent() })

}