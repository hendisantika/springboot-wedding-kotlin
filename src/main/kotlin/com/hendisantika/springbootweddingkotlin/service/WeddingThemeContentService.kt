package com.hendisantika.springbootweddingkotlin.service

import com.hendisantika.springbootweddingkotlin.entity.WeddingThemeContent
import com.hendisantika.springbootweddingkotlin.repository.WeddingThemeContentRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.PageRequest
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-wedding-kotlin
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-03-07
 * Time: 07:30
 */
@Service
@Transactional
class WeddingThemeContentService(
        @Autowired
        private val weddingThemeContentRepository: WeddingThemeContentRepository) :
        WeddingThemeContentRepository by weddingThemeContentRepository {

    fun findOrCreate(): WeddingThemeContent =
            findAll(PageRequest(0, 1)).elementAtOrElse(0, { WeddingThemeContent() })
}