package com.hendisantika.springbootweddingkotlin.service

import com.hendisantika.springbootweddingkotlin.entity.UserRole
import com.hendisantika.springbootweddingkotlin.repository.UserRoleRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-wedding-kotlin
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-03-07
 * Time: 07:26
 */
@Service
@Transactional
class UserRoleService(@Autowired private val userRoleRepository: UserRoleRepository)
    : UserRoleRepository by userRoleRepository, BulkDeleteService {

    override fun <S : UserRole?> save(entity: S): S {
        entity?.role = entity?.role?.toUpperCase() ?: ""
        return userRoleRepository.save(entity)
    }

    override fun <S : UserRole?> save(entities: MutableIterable<S>?): MutableList<S> {
        entities?.forEach { it?.role = it?.role?.toUpperCase() ?: "" }
        return userRoleRepository.save(entities)
    }

    override fun <S : UserRole?> saveAndFlush(entity: S): S {
        entity?.role = entity?.role?.toUpperCase() ?: ""
        return userRoleRepository.saveAndFlush(entity)
    }
}

interface BulkDeleteService {
    fun deleteAll(ids: LongArray): Int
}