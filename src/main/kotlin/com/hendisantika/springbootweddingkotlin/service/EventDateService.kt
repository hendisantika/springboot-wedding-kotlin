package com.hendisantika.springbootweddingkotlin.service

import com.hendisantika.springbootweddingkotlin.entity.EventDate
import com.hendisantika.springbootweddingkotlin.repository.EventDateRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import java.time.LocalDate

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-wedding-kotlin
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-03-07
 * Time: 07:29
 */
@Service
@Transactional
class EventDateService(
        @Autowired private val eventDateRepository: EventDateRepository) :
        EventDateRepository by eventDateRepository, BulkDeleteService {

    override fun <S : EventDate?> save(entity: S): S {
        entity?.date = LocalDate.parse(entity?.dateStr)
        return eventDateRepository.save(entity)
    }

    override fun <S : EventDate?> save(entities: MutableIterable<S>?): MutableList<S> {
        entities?.forEach { it?.date = LocalDate.parse(it?.dateStr) }
        return eventDateRepository.save(entities)
    }

    override fun <S : EventDate?> saveAndFlush(entity: S): S {
        entity?.date = LocalDate.parse(entity?.dateStr)
        return eventDateRepository.saveAndFlush(entity)
    }
}