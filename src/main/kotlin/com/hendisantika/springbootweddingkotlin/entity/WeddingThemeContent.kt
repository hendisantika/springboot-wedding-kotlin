package com.hendisantika.springbootweddingkotlin.entity

import org.hibernate.annotations.SortNatural
import java.util.*
import javax.persistence.*
import javax.validation.constraints.NotBlank

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-wedding-kotlin
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-03-05
 * Time: 19:58
 */
@Entity
data class WeddingThemeContent(
        @field: Id
        @field: GeneratedValue
        var id: Long? = null,

        @field: NotBlank(message = "{wedding.theme.content.header.required}")
        var aboutHeading: String = "",

        @field: Column(length = 4000)
        @field: NotBlank(message = "{wedding.theme.content.description.required}")
        var aboutDescription: String = "",

        @field: NotBlank(message = "{wedding.theme.content.header.required}")
        var examplesHeading: String = "",

        @field: NotBlank(message = "{wedding.theme.content.header.required}")
        var womenSubHeading: String = "",

        @field: NotBlank(message = "{wedding.theme.content.header.required}")
        var menSubHeading: String = "",

        @field: Column(length = 4000)
        @field: NotBlank(message = "{wedding.theme.content.description.required}")
        var womenDescription: String = "",

        @field: Column(length = 4000)
        @field: NotBlank(message = "{wedding.theme.content.description.required}")
        var menDescription: String = "",

        @field: OneToMany(targetEntity = PersistedFile::class, cascade = arrayOf(CascadeType.ALL), orphanRemoval = true, fetch = FetchType.EAGER)
        @field: SortNatural
        var womenExamplePics: SortedSet<PersistedFile> = TreeSet<PersistedFile>(),

        @field: OneToMany(targetEntity = PersistedFile::class, cascade = arrayOf(CascadeType.ALL), orphanRemoval = true, fetch = FetchType.EAGER)
        @field: SortNatural
        var menExamplePics: SortedSet<PersistedFile> = TreeSet<PersistedFile>(),

        @field: NotBlank(message = "{wedding.theme.content.header.required}")
        var themeInspirationHeading: String = "",

        @field: Column(length = 4000)
        @field: NotBlank(message = "{wedding.theme.content.description.required}")
        var themeDescription: String = "",

        @field: Column(length = 4000)
        @field: NotBlank(message = "{wedding.theme.content.youtube}")
        var youTubeLink: String = ""
)
