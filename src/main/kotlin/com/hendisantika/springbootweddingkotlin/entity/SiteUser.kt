package com.hendisantika.springbootweddingkotlin.entity

import com.hendisantika.springbootweddingkotlin.util.ValidPassword
import org.apache.commons.lang3.builder.EqualsBuilder
import org.apache.commons.lang3.builder.HashCodeBuilder
import javax.annotation.Nonnull
import javax.persistence.*
import javax.validation.constraints.Email
import javax.validation.constraints.NotBlank
import javax.validation.constraints.NotEmpty

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-wedding-kotlin
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-03-05
 * Time: 19:43
 */
@Entity
data class SiteUser(
        @field: Id
        @field: GeneratedValue
        var id: Long? = null,

        @field: NotBlank(message = "{user.username.blank}")
        @field: Column(unique = true)
        var userName: String = "",

        @field: NotBlank(message = "{user.email.blank}")
        @field: Email
        @field: Column(unique = true)
        var email: String = "",

        @field: NotBlank(message = "{user.password.blank}")
        @field: ValidPassword(message = "{user.bad.password}")
        var password: String = "",

        @field: Transient
        @field: NotBlank(message = "{user.password.blank}")
        @field: ValidPassword(message = "{user.bad.password}")
        var validatePassword: String = "",

        @field: Nonnull
        var enabled: Boolean = true,

        @field: Nonnull
        var accountNonExpired: Boolean = true,

        @field: Nonnull
        var credentialsNonExpired: Boolean = true,

        @field: Nonnull
        var accountNonLocked: Boolean = true,

        @field: ManyToMany(fetch = FetchType.EAGER, cascade = arrayOf(CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH, CascadeType.DETACH))
        @field: JoinTable(name = "user_roles",
                joinColumns = arrayOf(JoinColumn(name = "user_id", updatable = true)),
                inverseJoinColumns = arrayOf(JoinColumn(name = "role_id", updatable = true)))
        var roles: MutableSet<UserRole> = mutableSetOf(),

        @field: Transient
        @field: NotEmpty(message = "{user.roles.empty}")
        var roleIds: LongArray = longArrayOf()) {

    fun passwordMatch(): Boolean = password == validatePassword

    fun rolesString(): String = roles.joinToString(transform = { it.role })

    override fun equals(other: Any?): Boolean =
            EqualsBuilder.reflectionEquals(this, other)

    override fun hashCode(): Int =
            HashCodeBuilder.reflectionHashCode(this)
}

