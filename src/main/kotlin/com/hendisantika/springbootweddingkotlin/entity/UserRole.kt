package com.hendisantika.springbootweddingkotlin.entity

import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.Id
import javax.validation.constraints.NotBlank

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-wedding-kotlin
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-03-05
 * Time: 19:42
 */
@Entity
data class UserRole(
        @field: Id
        @field: GeneratedValue
        var id: Long? = null,

        @field: NotBlank(message = "{role.name.required}")
        @field: Column(unique = true)
        var role: String = ""
)