package com.hendisantika.springbootweddingkotlin.entity

import org.hibernate.validator.constraints.URL
import org.springframework.web.multipart.MultipartFile
import javax.persistence.*
import javax.validation.constraints.NotBlank
import javax.validation.constraints.NotNull

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-wedding-kotlin
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-03-05
 * Time: 20:01
 */
@Entity
data class Registry(
        @field: Id
        @field: GeneratedValue
        var id: Long? = null,

        @field: Column(unique = true)
        @field: NotBlank(message = "{registry.name.blank}")
        var name: String = "",

        @field: Column(unique = true)
        @field: NotBlank(message = "{registry.link.blank}")
        @field: URL(message = "{registry.link.invalid}")
        var link: String = "",

        @field: NotNull(message = "{registry.image.required}")
        @OneToOne(targetEntity = PersistedFile::class, cascade = arrayOf(CascadeType.ALL), orphanRemoval = true)
        var logoImage: PersistedFile = PersistedFile(),

        @field: Transient
        @field: NotNull(message = "{registry.image.required}")
        var uploadedFile: MultipartFile? = null)