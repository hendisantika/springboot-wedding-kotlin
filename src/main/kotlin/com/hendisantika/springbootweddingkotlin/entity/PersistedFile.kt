package com.hendisantika.springbootweddingkotlin.entity

import com.google.common.collect.ComparisonChain
import org.apache.commons.lang3.builder.EqualsBuilder
import org.apache.commons.lang3.builder.HashCodeBuilder
import javax.annotation.Nonnull
import javax.persistence.*
import javax.validation.constraints.NotBlank
import javax.validation.constraints.NotNull
import javax.xml.bind.DatatypeConverter

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-wedding-kotlin
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-03-05
 * Time: 19:53
 */
@Entity
data class PersistedFile(
        @field: Id
        @field: GeneratedValue
        var id: Long? = null,

        @field: NotBlank(message = "{persisted.file.name}")
        var fileName: String = "",

        @field: NotBlank(message = "{persisted.file.mime}")
        var mime: String = "",

        @field: NotNull(message = "{persisted.file.size}")
        var size: Long = 0,

        @field: NotNull(message = "{persisted.file.hash}")
        @field: Column(unique = true)
        var hash: Int? = null,

        @field: Nonnull
        @field: Lob
        var bytes: ByteArray? = null) : Comparable<PersistedFile> {

    override fun compareTo(other: PersistedFile): Int =
            ComparisonChain.start().compare(this.id ?: 0, other.id ?: 0).result()

    override fun equals(other: Any?): Boolean =
            EqualsBuilder.reflectionEquals(this, other)

    override fun hashCode(): Int =
            HashCodeBuilder.reflectionHashCode(this)

    fun asBase64(): String {
        val base64 = DatatypeConverter.printBase64Binary(bytes)
        return "data:$mime;base64,$base64"
    }
}