package com.hendisantika.springbootweddingkotlin.entity

import com.hendisantika.springbootweddingkotlin.util.ValidDate
import org.springframework.format.annotation.DateTimeFormat
import java.time.LocalDate
import java.time.temporal.ChronoUnit
import javax.persistence.*
import javax.validation.constraints.NotBlank
import javax.validation.constraints.NotNull

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-wedding-kotlin
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-03-05
 * Time: 19:56
 */
@Entity
data class EventDate(
        @field: Id
        @field: GeneratedValue
        var id: Long? = null,

        @field: DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
        @field: NotNull(message = "{eventDate.required}")
        var date: LocalDate = LocalDate.now(),

        @field: ValidDate(message = "{eventDate.required}")
        var dateStr: String = "",

        @field: NotBlank(message = "{eventDate.title.blank}")
        var title: String = "",

        @field: Enumerated(EnumType.STRING)
        @field: NotNull(message = "{eventDate.type.required}")
        @field: Column(unique = true)
        var dateType: com.hendisantika.springbootweddingkotlin.entity.DateType = com.hendisantika.springbootweddingkotlin.entity.DateType.Wedding) {

    fun calcRemainingDays(): Long {
        return ChronoUnit.DAYS.between(LocalDate.now(), date)
    }
}

enum class DateType { Wedding }