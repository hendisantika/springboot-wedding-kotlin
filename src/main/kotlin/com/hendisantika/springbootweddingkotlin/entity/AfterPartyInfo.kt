package com.hendisantika.springbootweddingkotlin.entity

import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.Id
import javax.validation.constraints.NotBlank

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-wedding-kotlin
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-03-05
 * Time: 20:00
 */
@Entity
data class AfterPartyInfo(

        @field: Id
        @field: GeneratedValue
        var id: Long? = null,

        @field: Column(length = 4000)
        @field: NotBlank(message = "{after.party.info.required}")
        var information: String = ""
)