package com.hendisantika.springbootweddingkotlin.entity

import org.springframework.web.multipart.MultipartFile
import javax.persistence.*
import javax.validation.constraints.NotBlank
import javax.validation.constraints.NotNull

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-wedding-kotlin
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-03-05
 * Time: 19:55
 */
@Entity
data class IndexCarousel(
        @field: Id
        @field: GeneratedValue
        var id: Long? = null,

        @field: NotNull(message = "{carousel.image.required}")
        @field: OneToOne(targetEntity = PersistedFile::class, cascade = arrayOf(CascadeType.ALL), orphanRemoval = true, fetch = FetchType.EAGER)
        var image: PersistedFile = PersistedFile(),

        @field: NotBlank(message = "{carousel.title.required}")
        @field: Column(unique = true)
        var title: String = "",

        @field: NotBlank(message = "{carousel.destination.required}")
        @field: Column(unique = true)
        var destinationLink: String = "",

        @field: NotNull(message = "carousel.order.required")
        @field: Column(unique = true)
        var displayOrder: Int = 0,

        @field: Transient
        @field: NotNull(message = "{carousel.image.required}")
        var uploadedFile: MultipartFile? = null
)