package com.hendisantika.springbootweddingkotlin.entity

import javax.persistence.*
import javax.validation.constraints.NotBlank

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-wedding-kotlin
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-03-05
 * Time: 19:58
 */
@Entity
data class WeddingVenueContent(
        @field: Id
        @field: GeneratedValue
        var id: Long? = null,

        @field: NotBlank(message = "{wedding.venue.title.blank}")
        var title: String = "",

        @field: NotBlank(message = "{wedding.venue.description.blank}")
        var description: String = "",

        @field: NotBlank(message = "{wedding.venue.google.maps}")
        @field: Column(length = 4000)
        var googleMaps: String = "",

        @field: OneToMany(targetEntity = PersistedFile::class, cascade = arrayOf(CascadeType.ALL), orphanRemoval = true, fetch = FetchType.EAGER)
        var images: MutableList<PersistedFile> = mutableListOf())
