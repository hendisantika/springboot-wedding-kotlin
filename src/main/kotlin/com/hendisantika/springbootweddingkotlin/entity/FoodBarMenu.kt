package com.hendisantika.springbootweddingkotlin.entity

import javax.persistence.*
import javax.validation.constraints.NotBlank

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-wedding-kotlin
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-03-05
 * Time: 20:00
 */
@Entity
data class FoodBarMenu(
        @field: Id
        @field: GeneratedValue
        var id: Long? = null,

        @field: NotBlank(message = "{foodbar.menu.title.required}")
        @field: Column(unique = true)
        var title: String = "",

        //This class has no meaning without the items so set the FetchType to EAGER
        @field: OneToMany(targetEntity = FoodBarMenuItem::class, fetch = FetchType.EAGER, cascade = arrayOf(CascadeType.ALL), orphanRemoval = true)
        var menuItems: MutableList<FoodBarMenuItem> = mutableListOf(),

        @field: Transient
        @field: NotBlank(message = "{foodbar.items.required}")
        var menuItemsStr: String = ""
)