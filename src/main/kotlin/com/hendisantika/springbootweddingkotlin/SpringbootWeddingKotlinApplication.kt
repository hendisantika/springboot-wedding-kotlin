package com.hendisantika.springbootweddingkotlin

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class SpringbootWeddingKotlinApplication

fun main(args: Array<String>) {
    runApplication<SpringbootWeddingKotlinApplication>(*args)
}
